## ==========================
## Paycent Raffles Web App
## Author: Jehnsen Enrique
## ==========================

## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```

### Compiles and hot-reloads for development
```
npm start
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

