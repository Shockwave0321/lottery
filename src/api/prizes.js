/* eslint-disable prettier/prettier */
export default [
    {
        index: 0,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/$1,000%20Paycent%20Preloaded%20Cards.png",
        desc: "$1,000 Paycent Preloaded Cards"
    },
    {
        index: 1,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/$2,500%20Paycent%20Preloaded%20Cards.png",
        desc: "$2,500 Paycent Preloaded Cards"
    },
    {
        index: 2,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/EVGA%20Geforce%20RTX%202080%20XC%20GAMING,%208GB%20GDDR6%20LED%20Graphics%20Card.png",
        desc: "EVGA Geforce RTX 2080 XC GAMING, 8GB GDDR6 LED Graphics Card"
    },
    {
        index: 3,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/Blink%20Xt2%20Security%20Camera%20System%20(5%20Camera%20Kit).png",
        desc: "Blink Xt2 Security Camera System (5 Camera Kit)"
    },
    {
        index: 4,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/2020%20Bentley%20Bentayga%20LUXURY%20SUV%20VEHICLE.png",
        desc: "2020 Bentley Bentayga LUXURY SUV VEHICLE"
    },
    {
        index: 5,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/GOLD%20BAR%20(1kg%2024kt%20w%20COA).png",
        desc: "1 KG 24 KARAT CERTIFIED GOLD BARS"
    },
    {
        index: 6,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/Rolex%20Cosmograph%20Daytona.png",
        desc: "Rolex Cosmograph Daytona"
    },
    {
        index: 7,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/Intel%20I9-9980xe%20Extreme%20Processor.png",
        desc: "Intel I9-9980xe Extreme Processor"
    },
    {
        index: 8,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/EPSON%20HOME%20CINEMA%205040UB%204KHDR%20PROJECTOR.png",
        desc: "EPSON HOME CINEMA 5040UB 4K/HDR PROJECTOR"
    },
    {
        index: 9,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/MISCROSOFT%20XBOX%20ONE%20X%201TB%204K%20ULTRA%20HDHDR.png",
        desc: "MISCROSOFT XBOX ONE X 1TB 4K ULTRA HD/HDR"
    },
    {
        index: 10,
        url : "https://paycent.com/wp-content/themes/twentysixteen-child/assets/img/blue/promo/prizes/SONY%20PLAYSTATION%20PRO%201TB.png",
        desc: "SONY PLAYSTATION PRO 1TB"
    }
]
