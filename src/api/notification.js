export default [
  {
    title: "PYN",
    color: "light-green",
    icon: "attach_money",
    timeLabel: "0.0125"
  },
  { divider: true, inset: true },
  {
    title: "BNB",
    color: "light-blue",
    icon: "attach_money",
    timeLabel: "3.124"
  },
  { divider: true, inset: true },
  {
    title: "PYN-C37",
    color: "cyan",
    icon: "attach_money",
    timeLabel: "0.0124"
  }
]
