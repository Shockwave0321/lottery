/* eslint-disable prettier/prettier */
import axios from "axios"

export class UserTickets {
  constructor() {}

  async getData() {
        
    let authToken = localStorage.getItem('auth');
    let wallet = localStorage.getItem('wallet');
    let email = localStorage.getItem('email');
  
    let response = await axios.get('account?auth=' + authToken + '&wallet=' + wallet + '&email=' + email)
      .then(res => {

          console.log(res.data)
      })

    return response.data
  }
  
}
