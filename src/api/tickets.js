/* eslint-disable prettier/prettier */
export default [
    {
        ticket: "100$ Card (2 BNB)",
        abbr: "A",
        price: "2 BNB"
    },
    {
        ticket: "250$ Card (5 BNB)",
        abbr: "B",
        price: "5 BNB"
    },
    {
        ticket: "iPhoneXR Lottery (10 BNB)",
        abbr: "C",
        price: "10 BNB"
    },
    {
        ticket: "Weekend Prizes Lottery (40 BNB)",
        abbr: "D",
        price: "40 BNB"
    },
    {
        ticket: "Rolex Daytona (250 BNB)",
        abbr: "E",
        price: "250 BNB"
    },
    {
        ticket: "One Kg of 24 Karat Certified Gold Bars (600 BNB)",
        abbr: "F",
        price: "600 BNB"
    },
    {
        ticket: "Bentley Bentayga (1100 BNB)",
        abbr: "G",
        price: "1100 BNB"
    }
  ]

  