/* eslint-disable prettier/prettier */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userAuth: null,
    userWallet: null,
    userEmail: null,
    userPassword: null,
    availablePYNBalance: 0,
    isVerified: false
  },
  mutations: {
    authUser(state, userData) {
      state.userAuth = userData.auth,
      state.userWallet = userData.wallet,
      state.userEmail = userData.email,
      state.userPassword = userData.password
    },

    clearAuth (state) {
        state.userAuth = null,
        state.userWallet = null,
        state.userEmail = null,
        state.userPassword = null
    },

    userDetails(state, userData) {
      state.availablePYNBalance = userData.pynbalance,
      state.isVerified = userData.verified
    }

  },
  actions: {

    login ({commit}, authData) {
 
        localStorage.setItem('email',authData.email)
        localStorage.setItem('wallet',authData.wallet)
        localStorage.setItem('password',authData.password)
        localStorage.setItem('auth',authData.auth)

        commit('authUser', {
            email: authData.email,
            wallet: authData.wallet,
            password: authData.passhash,
            auth: authData.auth
        })

    },

    registerUserToStore({commit}, userData) { 

      localStorage.setItem('wallet', userData.wallet)
      localStorage.setItem('email', userData.email)
      localStorage.setItem('password', userData.passhash)

      commit('authUser', {
        email: userData.email,
        wallet: userData.wallet,
        password: userData.passhash
      
      })

 
    },

    setAccountDetails({commit}, userData) {

      localStorage.setItem('verified', userData.verified)

      commit('userDetails', {
        pynbalance: userData.pynbalance,
        verified: userData.verified
      })

    },

    logout ({commit}) {
      localStorage.removeItem('auth');
      localStorage.removeItem('wallet');
      localStorage.removeItem('email');
      localStorage.removeItem('password');
      localStorage.setItem('verified', false);
      commit('clearAuth') 
    }

  },

  getters: {
      email (state) {
          return state.userEmail
      },
      password (state) {
          return state.userPassword
      },
      wallet (state) {
          return state.userWallet
      },
      auth (state) {
        return state.userAuth
      },
      verified (state) {
        return state.isVerified
      }
  }


})
