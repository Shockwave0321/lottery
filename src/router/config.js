import { AuthLayout, DefaultLayout } from "@/components/layouts"

export const publicRoute = [
  {
    path: "/",
    component: AuthLayout,
    meta: { title: "Login" },
    redirect: "/auth/login",
    hidden: true,
    children: [
      {
        path: "/",
        name: "login",
        meta: { title: "Login" },
        component: () => import("@/views/auth/Login.vue")
      }
    ]
  },
  {
    path: "/",
    component: AuthLayout,
    meta: { title: "Register" },
    redirect: "/auth/register",
    hidden: true,
    children: [
      {
        path: "/register",
        name: "register",
        meta: { title: "Register" },
        component: () => import("@/views/auth/Register.vue")
      }
    ]
  }
]

export const protectedRoute = [
  {
    path: "/dashboard",
    component: DefaultLayout,
    meta: { title: "Home", group: "apps", icon: "", requiresAuth: true },
    redirect: "/dashboard",
    children: [
      {
        path: "/dashboard",
        name: "dashboard",
        meta: { title: "Home", group: "apps", icon: "dashboard" },
        component: () => import("@/views/Dashboard.vue")
      }
    ]
  }
]
