/* eslint-disable prettier/prettier */
import Vue from "vue"
import "./plugins/vuetify"
import App from "./App.vue"
import Cookies from 'js-cookie'
import router from "./router/"
import store from "./store"
import "./registerServiceWorker"
import "roboto-fontface/css/roboto/roboto-fontface.css"
import "font-awesome/css/font-awesome.css"
import i18n from "./lang/lang"

import axios from "axios";
import moment from 'moment';
import VueClipboards from "vue-clipboards";
import { format } from "path"
import Carousel3d from "vue-carousel-3d";
import VTooltip from 'v-tooltip'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem('auth')) {
      next({
        name: 'login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (localStorage.getItem('auth')) {
      next({
        name: 'dashboard',
      })
    } else {
      next()
    }
  } else {
    next()
  }
  
})

// Production
axios.defaults.baseURL = "https://lottery.paycent.com/api/";
// axios.defaults.baseURL = 'https://staging-lottery-backend.paycent.co/api/';
axios.defaults.withCredentials = false;
axios.defaults.headers.post['Content-Type'] = 'application/json';

Vue.use(VueClipboards);
Vue.use(Carousel3d);
Vue.use(VTooltip);

Vue.filter('formatDate', function(value) {
  if (value) {
    var dateTime = new Date(value);
    return moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
  }
})

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app")

